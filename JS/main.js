///////////
// Edward C. Deaver, IV
// A1
// 9/7/17
//////////
////////////////////////////////////////////
// Description: Mock demonstration of getting JSON file. Due to cross origin policies in Chrome a local JSON file can not be loaded unless the site is hosted on a webserver or on gitlab/github pages
////////////////////////////////////////////
// Pre:  Date given
// Post: json object returned
////////////////////////////////////////////
function retrieveEventData(dateToGet) {
    // The form sends the date as YEAR - MONTH - DAY
    var sampleResponse = '{"DATE":"2018-12-25","EVENTS":[{"TIME": 1230,"DURATION":120,"DESCRIPTION":"Going to family dinner."}, {"TIME": 1730,"DURATION":20,"DESCRIPTION":"Workout"}]}';
    var obj = JSON.parse(sampleResponse);
    console.log(obj);
    return obj;
}
////////////////////////////////////////////
// Description: This function Loads the json data and creates a nested list for each event time with it's data
// Note: in future this function should be a callback function of retrieveEventData and not a solo function
////////////////////////////////////////////
// Pre:  Event Data recieved
// Post: Elements created
////////////////////////////////////////////
var currentDate;

function fillEventData(jsonResponseData) {

    var response = retrieveEventData();
    console.log(response);
    console.log(response.DATE);
    console.log(document.getElementById('EVENTS').getElementsByTagName('ul').length);
    if (document.getElementById('EVENTS').getElementsByTagName('ul').length > 0) {
        console.log(document.getElementById('eventList').innerHTML);
    } else {

        // CREATE DATE LABEL

        var eventList = document.createElement('ul');
        eventList.setAttribute('id', 'eventList');
        document.getElementById('EVENTS').appendChild(eventList);
        eventList.innerHTML = response.DATE;

        var timeEvent = document.createElement('ul');
        timeEvent.setAttribute('id', 'timeEvent');
        eventList.appendChild(timeEvent);


        for (i = 0; i < jsonResponseData.EVENTS.length; i++) {

            var timeLabel = document.createElement('li');
            timeLabel.setAttribute('class', 'itemTitle');
            timeEvent.appendChild(timeLabel);
            timeLabel.innerHTML = "Event Time: " + jsonResponseData.EVENTS[i].TIME;

            var eventData = document.createElement('ul');
            timeLabel.setAttribute('class', 'eventData');
            timeLabel.appendChild(eventData);


            var duration = document.createElement('li');
            duration.setAttribute('class', 'item');
            eventData.appendChild(duration);
            duration.innerHTML = "Event Duration: " + jsonResponseData.EVENTS[i].DURATION;
            var li = document.createElement('li');
            li.setAttribute('class', 'item');
            eventData.appendChild(li);
            li.innerHTML = "Event Duration: " + jsonResponseData.EVENTS[i].DESCRIPTION;
        }
    }

}


